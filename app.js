
const scaleNames = {
    c: 'Celsus',
    f: 'Fahrenheit'
}
function BoillingVerdict({celsus})
{
    if(celsus >= 100){
        return <div className="alert alert-success">L'eau bout</div>
    }else{
        return <div className="alert alert-info">L'eau ne bout pas</div>
    }
}   

function toCelsus(fahrenheit) {
    return (fahrenheit - 32) * 5 / 9
}

function toFahrenheit(celsus) {
    return (celsus * 9/5) + 32
}

class TemperatureInput extends React.Component{
    constructor (props){
        super(props)
        // this.state = {temperature: ''}
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange (e){
        // this.setState({temperature: e.target.value})
        // console.log(this.state.temperature)
        this.props.OnTemparatureChange(e.target.value)
    }

    render (){
        const name = 'scale' + this.props.scale
        const scaleName = scaleNames[this.props.scale]
        const {temperature} = this.props
        return <div className="form-group">
            <label htmlFor={name}>Température en ({scaleName})</label>
            <input type="text" id={name} className="form-control" value={temperature} onChange={this.handleChange}></input>
         </div>
    }
}

class Calculator extends React.Component{
    constructor (props){
        super(props)

        this.state ={
            scale: 'c',
            temperature:20
        }
        this.handleCelsusChange     = this.handleCelsusChange.bind(this)
        this.handleFahrenheitChange = this.handleFahrenheitChange.bind(this)
    }

    handleTemparatureChange(temperature){
        this.setState({temperature})
    }

    // handleChange (e){
    //     this.setState({temperature: e.target.value})

    //     console.log(this.state.temperature)
    // }
    handleCelsusChange(temperature){
        this.setState({
            scale: 'c',
            temperature
        })
    }
    handleFahrenheitChange(temperature){
        this.setState({
            scale: 'f',
            temperature
        })
    }
    render() {
        const {temperature, scale} = this.state
        const celsus = scale === 'c' ? temperature : toCelsus(temperature)
        const  fahrenheit = scale === 'f' ? temperature : toFahrenheit(celsus)
        return <div>
                        {/* <label htmlFor="celsus">Température en celsus</label>
                        <input type="text" id="celsus" className="form-control" value={this.state.temperature} onChange={this.handleChange}></input> */}
                        <TemperatureInput scale="c" temperature= {celsus} OnTemparatureChange={this.handleCelsusChange}/>
                        <TemperatureInput scale="f" temperature= {fahrenheit} OnTemparatureChange={this.handleFahrenheitChange}/>
                        <BoillingVerdict celsus={parseFloat(temperature)}/>
               </div>
    }
}
ReactDOM.render(<Calculator/> , document.querySelector('#app'))